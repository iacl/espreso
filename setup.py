from setuptools import setup

version = "2.3.5"

with open("README.md") as readme:
    long_desc = readme.read()

setup(
    name="espreso",
    description="An algorithm to estimate an MR slice profiles by learning "
    "to match internal patch distributions",
    author="Shuo Han",
    author_email="shan50@jhu.edu",
    version=version,
    packages=["espreso"],
    license="GPLv3",
    python_requires=">=3.7.10",
    entry_points={
        "console_scripts": [
            "espreso-train=espreso.exec:train",
            "espreso-fwhm=espreso.exec:fwhm",
        ]
    },
    long_description=long_desc,
    install_requires=[
        "torch>=1.8.1",
        "numpy",
        "scipy>=1.6.3",
        "nibabel>=3.2.1",
        "matplotlib>=3.4.2",
        "tqdm",
        "scikit-image>=0.18.1",
        "improc3d>=0.5.2",
        "ptxl@git+https://gitlab.com/shan-deep-networks/ptxl@0.3.2",
        "sssrlib@git+https://github.com/shuohan/sssrlib@0.3.0",
    ],
    long_description_content_type="text/markdown",
    url="https://gitlab.com/iacl/espreso",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
)
