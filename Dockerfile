FROM pytorch/pytorch:1.8.1-cuda10.2-cudnn7-runtime
RUN apt-get update && apt-get install -y git

COPY . /tmp/espreso

RUN pip install /tmp/espreso && \
    mkdir -p /tmp/matplotlib && \
    rm -rf /tmp/espreso 

ENV MPLCONFIGDIR=/tmp/matplotlib
CMD ["bash"]
