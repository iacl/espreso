# ESPRESO2: Estimate the Slice Profile for Resolution Enhancement from a Single Image Only Version 2

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

| **[Paper][paper]** |

## Introduction

This algorithm estimates a slice profile from a single 2D MR acquisition. 2D MR acquisitions usually have a lower through-plane resolution than in-plane resolutions. The rationale of ESPRESO2 is that if we use a correct slice profile to blur a high-resolution in-plane axis, its appearance should match the low-resolution through-plane axis. Therefore, we use a GAN to learn this slice profile by matching the distributions of image patches that are extracted along the in- and through-plane axes.

<img src="docs/source/_static/images/flowchart.svg" width="600"/>

**Figure 1**: Flowchart of ESPRESO2. **G**: the GAN's generator. **D**: the GAN's discriminator. **T**: transpose.

This algorithm can be used to
* create training data for self-supervised super-resolution algorithms (SSR, [Jog 2016](https://pubmed.ncbi.nlm.nih.gov/29238758/), [Zhao 2020](https://pubmed.ncbi.nlm.nih.gov/33170776/)) that improves the through-plane resolution,
* measure the the difference between in- and through-plane resolutions (as a metric of the performance of these SSR algorithms).

Example results of using it with [Zhao 2020](https://pubmed.ncbi.nlm.nih.gov/33170776/) are shown in Fig. 2. Example measurements of through-plane resolutions (relative to the in-plane resolutions) are shown in Fig. 3.

<img src="docs/source/_static/images/ismore.svg" width="600"/>

**Figure 2**: Example results of an SSR algorithm with and without ESPRESO2. The true slice profile (red) and our estimated slice profile (blue) are shown on the right. The yellow arrow points to an artifact.

<img src="docs/source/_static/images/measure.svg" width="600"/>

**Figure 3**: Example measurements of through-plane resolutions. **(A)**: the true isotropic image. **(B)**, **(C)**: the super-resolved images from simulations with blurs of FWHM = 2 and FWHM = 4 pixels, respectively, and their corresponding estimated slice profiles from ESPRESO2. Their FWHMs (1.92 and 2.85) can be used as measurements of the super-resolution performance. Yello arrows point to the differences between these images.

## Installation

From the project root directory:

```bash
pip install .
```


## Usage

First, select which GPU you want to run it on, and replace `$GPU_ID` with that integer.

If `espreso` is installed in the host machine, run

```bash
CUDA_VISIBLE_DEVICES=$GPU_ID; espreso-train -i $image -o $output_dir
```


To use the Singularity image, run
```bash
CUDA_VISIBLE_DEVICES=$GPU_ID; singularity run -B $image:$image -B $output_dir:$output_dir --nv \
    espreso2_031.sif espreso-train -i $image -o $output_dir
```


[paper]: https://arxiv.org/pdf/2104.00100.pdf
